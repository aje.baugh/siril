siril 1.2.x
xx/xx/22

* Added livestacking mode with darks/CC/flats support, registration and stacking
* Added unlinking channels in autostretch preview
* Added RGB equalization and lightweight (faster) normalisation in stacking options
* Added LRGB composition command and PixelMath command for new offline compositions
* Added Starnet++ integration in GUI and command and mask creation
* Added Star Recomposition tool to mix and stretch starless and starmask images
* Added star intensive care to unsaturate stars
* Added new deconvolution tool with RL, Split Bregman and Wiener algorithms and several PSF generation options (replaces old tool)
* Added new denoising tool
* Added pcc command for headless PCC and plate solving
* Added local KStars star catalogue support for offline astrometry and PCC (NOMAD)
* Added a new framing assistant with several modes and image framing preview
* Added specifying max number of stars for registration
* Added bad pixel map option for preprocess, both in GUI and command
* Added and reviewed commands for offline and automatic post-processing
* Added background level and star count as new sequence filters
* Added option to compute sequence filtering threshold using k-sigma clipping
* Added weighing based on number of stars or wFWHM for stacking
* Added a new threading mechanism for per-image improved performance
* Added star selection from coordinates to seqpsf and star tracking with homography
* Added headless light curve creation from a list of star equatorial coordinates
* Added star list importing from the NINA exoplanet plugin for light curve creation
* Added relaxed mode for star detection for particularly bad images
* Added crop to selection to the rotation geometric transform
* Added a way to clean sequence file in the sequence tab (see also command seqclean)
* Added a warning when images are negative on calibration
* Added calibration details in the FITS history
* Added saving formulas (presets) in Pixel Math
* Added statistic functions to Pixel Math as well as mtf
* Added solar system object search from online service for image annotations
* Added zoom sliders and plot selection in Plot tab
* Added Moffat star profile as an option instead of Gaussian
* Added the possibility to run statistics on individual filters of CFA images
* Added parsing paths with header key values for preprocess, stack and save actions
* Added a high definition mode to auto-stretch visualization
* Added memory and processor count cgroups limits enforcement (linux only)
* Added a mapping file created by conversion operations
* Added background level and number of stars to stored registation data and plots
* Added commands: [update with https://free-astro.org/index.php?title=Siril:Commands#Commands_history ]
* Added KOMBAT alogrithm for registration and removed deprecated ECC
* Added choosing server to query object in astrometry
* Added shortcuts for astrometry (regular and photometric correction)
* Added option "full" to export all possible stats in seqstat command
* Added argument to executable to pass pipes path, allowing several instances to run simultaneously
* Added more reporting on online object search, avoiding duplicates in catalogues
* Added improved graphical interface to hyperbolic stretches
* Added 9-panel dialog tool showing corners and center of the image for a closer inspection
* Added NL-Bayes denoising algorithm with optional boosters DA3D, SOS and Anscombe VST
* Added undershoot clamping to bicubic and lanczos4 interpolation methods
* Added 9-panel dialog tool showing corners and center of the image for a closer inspection
* Added NL-Bayes denoising algorithm with optional boosters DA3D, SOS and Anscombe VST
* Added undershoot clamping to bicubic and lanczos4 interpolation methods
* Added CFA merge process for reconstructing a Bayer pattern previously split out with extraction
* Added binning and binxy command
* Added rejection maps creation on rejection stacking
* Added astro-tiff option in the command savetif with -astro
* Allowed area selection (and much more) to be done on RGB display tab
* Updated scripts to specify cosmetic correction is from masterdark with -cc=dark option
* Updated seq file version to v4. Registration block keeps homography information
* Updated behaviour of channel select toggles in histogram and hyperbolic stretch tools, allowing stretching only selected channels
* Replaced Libraw with RT debayering
* Replaced generalized hyperbolic transform stretch method by more general algorithms and optimised for speed
* Optimised asinh code for speed
* Improved Ha-OIII extraction to offer full resolution O-III output and improve star roundness
* Improved management of focal length, pixel size and binning, in settings and images
* Refactored global registration and added 2pass and Apply Existing methods
* Refactored 3-star registration to run the 3 stars analysis successively
* Refactored 1- and 2/3-stars registration into one single registration method
* Refactored PCC, using WCS information to identify stars instead of registration match
* Refactored settings, preferences and configuration file, adding get and set commands
* Refactored commands error handling
* Refactored light curve creation, filtering the reference stars to valid only
* Refactored PSF fitting to solve for the angle in all cases
* Refactored astrometry for command and sequence operation, astrometry.net interop.
* Fixed comet registration when registration was accumulated over existing data
* Fixed star detection for images with very large stars
* Fixed cancellation of seqpsf or seqcrop
* Fixed photometry analysis of stars with negative pixels around them
* Fixed dates keywords in sum and min/max algorithms
* Fixed FITS header preservation on RGB composition
* Fixed possible FITSEQ infinite loop and inconsistent numbering caused by hidden files in conversion
* Fixed sequence closing in script processing after each command
* Fixed opening of scientific FITS cube files
* Fixed gnuplot call for macOS/AppImage, by making its path configurable
* Fixed bug in roworder of SER sequence created from TOP-DOWN FITS images
* Added ability to use Starnet++ on sequences
* Fixed bug when saving 16bits signed FITS images
* Fixed internationalization in siril-cli
* Fixed subsky command success status

siril 1.0.6
10/18/22

* Fixed crash on opening a malformed SER
* Fixed crash in savetif
* Fixed crash in polynomial background extraction when not enough sample were set
* Fixed bug in iif where no parameters could be used
* Fixed crash in seqstat when some images were deselected
* Fixed crash in star detection when large star was close to border
* Fixed bad behaviour of asinh blackpoint with monochrome/32bits images
* Fixed bug in PixelMath with negate values
* Fixed bug in SIMBAD request when object name contains '+'
* Fixed bug in rotational gradient

siril 1.0.5
09/09/22

* Fixed bug in eyedropper feature with 16bits images
* Added button to see original image in background extraction
* Fixed bug introduced in 1.0.4 with one pixel shift when registering meridian flipped images
* Fixed GAIA catalog parser

siril 1.0.4
09/02/22

* Fixed selected area for flat autonormalisation calc
* Fixed wrong initialization in polynomial background extraction
* Fixed cold pixels rejection in GESDT stacking
* Fixed x and y position in PSF and star detection
* Fixed RGB pixel display where Green and Blue were swapped
* Fixed random crash in Pixel Math
* Improved wcs import when formalism used is deprecated (CROTA + CDELT)
* Added dropper icon to set SP easily in GHT tool

siril 1.0.3
06/28/22

* Fixed memory leak in PixelMath
* Fixed memory leak in SER preview
* Fixed error in seqsubsky
* Fixed the start of two scripts from @ in succession
* Fixed homogeneous image bitpix detection on stacking
* Fixed dates in median and mean stack results
* Fixed bug in stack of some float images
* Fixed black point and clipping in asinh stretch function
* Added new thread for background extraction that does not freeze UI, with a progressbar for RBF
* Added generalised hyperbolic transform stretch method in Image Processing menu, based on algorithms proposed by David Payne in the astrobin forums

siril 1.0.2
05/16/22

* Added new RBF interpolation method in the background extraction tool
* Removed file name length limit in conversion
* Fixed crash in preprocess command if a quote was not closed in -bias= option
* Fixed crash when star detection box was expanded past a border
* Fixed crash in plot when X axis data contained negative values
* Fixed numerous bugs in the background extraction tool
* Fixed bug in PixelMath where only one char parameter where allowed
* Fixed bug in FITS partial reader

siril 1.0.1
04/06/22

* Added min,max, iif and logical operators in pixelmath
* Added support for 3 channels for direct preview of resulting composition in pixelmath
* Added parameters field in PixelMath
* Added reporting channel name in FILTER header key during split operation
* Added using FILTER keyword value for Pixel Math tool variable name
* Fixed using shift transform for global registration
* Fixed crash when changing image with preview opened
* Fixed crash when pressing Stop button during script execution
* Fixed crash that could occur when moving the mouse over the image while it was being updated
* Fixed date wrongly reported in the FITS header in SER/FITSEQ stacking when filtering out images
* Fixed excluding null pixels of both ref and target in linear match

siril 1.0.0
03/09/22

* Added ASTRO-TIFF standard
* Fixed memory consumption for all sequence operations
* Fixed settings for sequence export as webm film with VP9 codec
* Removed use of lo/hi cursors and fixed normalization for export
* Fixed load and close commands for scripts in GUI
* Fixed Bayer pattern in SER after extraction
* Fixed registration crash for small images
* Improved main panel separator positioning and keeping it in memory
* Improved speed of FITSEQ detection when scanning sequences
* Improve usability on MacOS
* Reintroduced compatibility with OpenCV 4.2 with disabled features

siril 1.0.0~RC2
12/08/21

* Fixes many crashes
* Minor improvements in plot feature
* Restore HD for macOS application

siril 1.0.0~RC1
11/20/21

* New Pixel Math tool
* New plot tool to visualize and sort sequence based on any registration data field available
* New tilt estimation feature (from GUI or command)
* New SNR estimator in photometry analysis
* New button for quick photometry
* New commands seqcosme and seqcosme_cfa to remove deviant pixels on sequence, using file computed with find_hot
* New command boxselect to specify a selection box with x, y, w and h
* Improved candidates star detection speed and accuracy with a new algorithm
* Reviewed GUI and improved responsiveness
* Saving focal and WCS data in the swap file using undo/redo
* WCS data is now updated after geometric transformations (mirror, rotate and crop)
* Seqcrop command can now be used in scripts
* Added autocropping of wide-field images, as well as using a selection for plate solving
* Added choices of degrees of freedom (shift, similitude, affine or homography) for global registration
* Added UI button to plot WCS grid and compass
* Added user catalogue for astrometry annotation
* Added GAIA EDR3 catalogue for astrometry
* Added choice between clipboard and file for snapshot
* Added equalize CFA for X-Trans sensor
* Allowing debayer to be forced for SER files
* Converted constant bitrate quality to constant quality rate settings in sequence export to film
* Fixed memory leak in opencv that made registration of FITS files fail often
* Fixed FWHM units and star center position in Dynamic PSF
* Fixed global registration with various image sizes
* Fixed bug in ECC algorithm

siril 0.99.10.1
06/23/21

* Fixed star detection with resolution < 1.0
* Fixed interpolation issue in global registration
* Fixed timestamp issue with glib < 2.66
* New MAD clipping algorithm

siril 0.99.10
06/11/21

* New drag and drop
* New presets for sequences export
* New choice between h264 and h265 for mp4 export
* New Generalized Extreme Studentized Deviate Test as a new rejection algorithm
* New weighted mean stacking based on bgnoise
* New independent normalization of each channel for color images
* New faster location and scale estimators to replace IKSS with similar accuracy
* New synthetic level for biases
* New 2- or 3-star registration algorithm with rotation
* New SER debayering at preprocess
* New green extraction from CFA
* New option to downsample image while platesolving
* Remember focal and pixel size in astrometry tool
* Updated sampling information after channel extraction and drizzle
* Fixed bands appearing on mean stacking for CFA SER sequences
* Fixed bug in FFT filter
* Fixed bug in statistics and normalization for 16b images
* Changed handling of zero values in statistics, global registration, normalization and stacking

siril 0.99.8.1
02/13/21

* Fixed crash because of wcslib function

siril 0.99.8
02/10/21

* New ability to remove sequence frames from the "Plot" tab
* New merge command
* New astrometry annotation ability
* New snapshot function
* New conversion internal algorithm, can convert any sequence to any other sequence type too now
* Handle datetime in TIFF file
* Improved color saturation tool with a background factor to adjust the strength
* Reduced memory used by global registration
* Improving films (AVI and others) support: notifying the user, suggesting conversion, fixing parallel operations
* Fixed memory leak in minmax algorithms
* Fixed a bug in FITS from DSLR debayer when image height is odd
* Fixed out-of-memory conditions on global registration and median or mean stacking
* Fixed SER stacking with 32 bits output
* Fixed bitrate value issue in mp4 export
* Fixed normalization issue with SER files

siril 0.99.6
09/23/20

* Selection can be moved and freely modified, its size is displayed in UI (Sébastien Rombauts)
* Undo/Redo buttons now display the operations they act upon (Sébastien Rombauts)
* Added color profile in TIFF and PNG files
* Image display refactoring (Guillaume Roguez)
* Fixed a bug in demosaicing orientation
* Fixed a bug in macOS package where Siril was not multithreated
* Fixed memory leak in pixel max/min stacking
* Fixed crash when selecting 1 pixel
* Better integration in low resolution screen
* Added embed ICC profile in png and TIFF files
* By default Siril now checks update at startup
* By default Siril now needs “requires” command in Script file
* Refactoring of image display with pan capacity
* Added button + and – for zooming management

siril 0.99.4
08/14/20

* New UI with a single window
* New demosaicing algorithms, RCD is now the default one
* New algorithm to fix the AF square with XTRANS sensor (Kristopher Setnes)
* New support for FITS decompression and compression with Rice/Gzip and HCompress methods (Fabrice Faure)
* New support for quantization and HCompress scale factor settings for FITS compression (Fabrice Faure)
* New functions to extract Ha and Ha/OII from RGB images
* New linear match function
* New link command to create symbolic links
* New convert command to convert all files (and link FITS)
* New preference entries for FITS compression settings (Fabrice Faure)
* New native image format: 32-bit floating point image
* New native sequence format: FITS sequence in a single image
* New UI for sequence image list
* New zoom handing: ctrl+scroll (up and down) is the new way to zoom in and out
* New preview in open dialog
* New language selector in preferences
* New image importer: HEIF format
* New stacking filtering criterion (weighted FWHM). It can exclude more spurious images
* New macOS bundle
* New RL deconvolution tool
* New keyword CTYPE3 for RGB FITS in order to be used by Aladin
* New binary siril-cli to start siril without X server
* New preference entries with darks/biases/flat libraries
* New preliminary Meson support (Florian Benedetti)
* New ROWORDER FITS keyword that should be used by several programm now
* X(Y)BAYEROFF can now be configured in preferences
* Parallelizing conversion and some other functions
* CI file was totally rewritten (Florian Benedetti)
* Config file was moved to more standard path
* Optimization of several algorithms (Ingo Weyrich)
* Background extraction is now available for sequence
* Midtone Transfer Function is now available for sequence
* Fixed code for Big Endian machine (Flössie)
* Fixed bug in SER joining operation when Bayer information was lost
* Fixed a bug of inaccessible directories in MacOS Catalina
* Fixed crash on some OpenCV operation with monochrome images
* Fixed annoying error boxes about missing disk drives on Windows

siril 0.9.12
11/04/19

* Fixed stat computation on 3channel FITS
* Fixed free memory computation on Windows
* Fixed a bug in RGB compositing mode allowing now users to use multichannel image tools
* Fixed crash after deconvolution of monochrome images
* Fixed a bug in astrometry when downloaded catalog was too big
* New split cfa feature
* Script status (line currently executed) is displayed in a statusbar
* TIFF export is now available for sequences
* Better dialog windows management
* Histogram tool refactoring
* Provide new strategies for free memory management
* Provide new photometric catalog for color calibration (APASS)
* Added new filter: Contrast Limited Adaptive Histogram Equalization
* Open sequence by double clicking on seq file

siril 0.9.11
05/27/19

* New icon set
* New photometric color calibration tool
* New background extraction tool working with 64-bit precision and dither
* Improved processing speed by optimizing sorting algorithms to each use
* Parallelizing preprocessing
* New image filtering for sequence processing: possible from the command line and with multiple filters
* Improved free disk space feedback and checks before running preprocess, global registration and up-scaling at stacking
* New GTK+ theme settings: it can now be set from siril to dark or light, or kept to automatic detection
* New normalization to 16 bits for RAW images with less dynamic range (in general 12 or 14)
* Improved mouse selection by making it more dynamic
* Added drag and drop capability in the conversion treeview
* Added output file name argument to stacking commands
* New command setmem to limit used memory from scripts
* New clear and save buttons for the log in GUI
* Taking into account the Bayer matrix offset keywords from FITS headers
* Displaying script line when error occurs
* Allow registration on CFA SER sequences
* Processing monochrome images requires less memory, or can be more paralellized if memory was the limiting factor
* Fixed dark optimization
* Fixed crash in global registration on a sequence that contained a dark image
* Fixed management of the statistics of images on which they fail to be computed
* Fixed free disk space detection and usual processing commands on 32-bit systems
* Fixed free memory detection for stacking in case of up-scaling ('drizzle') and memory distribution to threads
* Fixed bug in FFT module
* Fixed bug in the drawn circle of photometry
* Fixed build fail with OpenCV 4.0.1
* Fixed SER sequence cropping
* Fixed regression in global registration for images having different size
* Added German translation

siril 0.9.10
01/16/19

* New astrometry tool that solves acquisition parameters from stars in the image (requires Web access and libcurl)
* New comet registration method
* Enabled previews for color saturation, asinh stretching, histogram transform and wavelets
* Added ability to join SER files
* Added a command stream using named pipes
* Added RGB flat normalisation for CFA images and command GREY_FLAT
* Added SETFINDSTAR command to define sigma and roundness parameters
* Added ASINH command and GUI function, for asinh stretching of images
* Added RGRADIENT command and GUI function
* Added negative transformation
* Made command SEQCROP scriptable
* Improved ECC alignment algorithm
* Improved global registration and fixed a bug
* Redesigned all dialog windows to conform to GNOME guidelines
* Preserving history in FITS file header when former history exists
* Preserving FITS keywords in sum stacked image
* Checking and increasing if needed maximum number of FITS that can be stacked on the system
* Automatically detecting GTK+ dark theme preference
* Adding a setting to activate image window positioning from the last session
* Fixed a bug in photometry where stars were too round
* Fixed an issue with wide chars on Windows
* Fixed some erratic behaviour when reference image was outside selection
* Fixed default rotation interpolation algorithm for register command
* Fixed a crash on sequence export with normalization
* Fixed line endings in scripts for all OS
* Fixed compilation for OpenCV 4.0
* Fixed dark optimization and added -opt option in PREPROCESS command
* Fixed a crash in global registration with unselected images

siril 0.9.9
06/07/18

* Major update of the command line, with completion and documentation in the GUI, enhanced scripting capability by running commands from a file and also allowing it to be run with no X11 server running with the -s command line option
* Added commands to stack and register a sequence
* Image statistics, including auto-stretch parameters and stacking normalization, are now cached in the seq file for better performance
* Global star registration now runs in parallel
* Workflow improvement by adding demosaicing as last part of the preprocessing
* Added a filtering method for stacking based on star roundness
* Added an option to normalize stacked images to 16-bit with average rejection algorithm
* All GUI save functions are now done in another thread
* Improved histogram transformation GUI
* Improved support of various FITS pixel formats
* Preserve known FITS keywords in the stacked image by average method
* Added native open and save dialogues for Windows users
* Various Windows bug fixes in SER handling
* Fixed wrong handling of scale variations in Drizzle case
* Fixed 8-bit images auto-stretch display
* Fixed BMP support
* Fixed issues in PNG and TIFF 8-bit export
* Fixed the "About" OS X menu

siril 0.9.8.3
02/19/18

* Check for new available version
* Handle XTRANS FUJIFILM RAWs
* Fixed Preprocessing SER files that gave corrupted SER results
* Fixed SaveBMP that added tif extension
* Fixed Registration on all images that was done on selected images instead
* Fixed Target directory that was ignored when saving as image
* Fixed crash with Wrong SER timestamp

siril 0.9.8
01/31/18

* Added SavePNG
* Allow to use gnuplot on Windows if it is installed on the default path
* Improve SER processing speed
* Opencv is now mandatory
* Implementation of a simplified Drizzle
* New tool for Lucy-Richardson deconvolution
* Conversion list tree is now sorted on first load. Sort is natural.
* Command stackall is available, with optional arguments, for all stacking methods
* Change default working directory to special directory 'Pictures' if it exists
* Reduce display time of autostretch
* Parallelize sum stacking
* Use thread-safe progress bar update instead of deprecated one. Run 'Check sequences' in a background task
* Add an option to set the generic image_hook behaviour when function fails
* Switch on "Images previously manually selected from the sequence" if user checks and unchecks frames
* Fixed numerous bug on Windows with wide char filenames
* Fixed dark theme icons
* Fixed exposure dates of exported SER sequences that were wrong with filtering
* Fixed the loss of color calibration on background extraction
* Fixed menu update after RGB composition
* Fixed bug in "Average" and "Median" stack for huge SER file
* Fixed when it was impossible to use multithread functions after star alignment in compositing tool
* Fixed crash when selecting "Best images .. (PSF)" if the loaded sequence has no PSF data
* Fixed sorted images by FWHM
* Fixed crash on stacking when no reference image is selected and first image of the sequence is excluded

siril 0.9.7
09/21/17

* Fixed French translation
* Fixed bug in registration from compositing for layers alignment
* Fixed crash when stacking failed
* Fixed limit of 4Go SER file for non POSIX Standard
* Improved global registration. New algorithm with homography

siril 0.9.6
06/20/17

* Allow sequence export to use stacking image filtering
* Get the working directory as an optional command line argument
* Improve photometry
* Fixed wrong selected image in list panel when list was sorted
* Fixed registration with unselected images which made progress bar exceed 100%
* Fixed again compilation that failed on KFreeBSD
* Fixed name of Red Layer using compositing tool that was wrong

siril 0.9.5
11/28/16

* Implement a graph interface to display quality registration information
* No X and Y binning value could lead to errors with fwhm
* Take reference image as normalisation reference
* Retrieve Bayer pattern from RAW file
* Export sequence to MP4
* Statistics should not take into account black point
* Add ComboBox for registration interpolation
* Fixed interpolation in global star registration that gave blurred results
* Fixed FreeBSD intltool compilation fails
* Fixed erroneous created sequence in registration with unselected images
* Fixed compilation that failed on KFreeBSD

siril 0.9.4
08/17/16

* Fixed issues with SER in generic processing function
* Fixed inability to open FITS when filename had parenthesis
* Fixed selecting new images did not update the number of selected images
* Fixed histogram sliders lag on OS-X
* Fixed message "Error in highest quality accepted for sequence processing....." during stack of %, even if quality data are computed
* Fixed sequence export to SER with unselected images
* Fixed global star alignment with angle close to 180deg
* Fixed undo cosmetic correction
* Fixed crash in peaker function
* Fixed aborting global star registration summary
* Fixed sequence list which was unreadable with dark GTK theme
* Fixed the update of the list of images
* Added support of internationalization: French, Dutch, Chinese, Italian, Arabic
* Option for logarithm scale in histogram transformation
* Add siril.desktop in archlinux
* Added support for exporting sequence in avi format
* Option to make a selection for global star registration in a smaller region
* Read commands from a file
* Option to follow star in registration
* Added support for resizing exported sequence
* Added support for reading and writing SER timestamps
* Added support for RGB alignment
* Added functionality to fix broken (0 framecount) SER files.

siril 0.9.3
04/16/16

* Fixed bug in preprocessing
* Fixed compilation error in some conditions
* Fixed uninitialized values
* Fixed typos

siril 0.9.2
04/04/16

* Added support for dark optimization
* Added hot pixel removal feature
* Added Animated GIF output and registered sequence export
* Added autostretch viewer mode
* Allowing a reference magnitude to be set to get absolute magnitude instead of relative
* New commands: sequence selection range and stackall
* Added vertical banding noise removal tool
* Providing a better planetary registration algorithm
* Parallelized registration
* Refactored conversion to include AVI to SER
* Configurable "Are you sure" dialogues
* ls command gives results in an ordered way
* Updated to FFMS2 latest version
* Clarified the use of demoisaicing
* Improved star detection
* Improved RGB compositing tool
* Allowing manual selection of background samples
* Fixed force recomputing statistics for stacking
* Fixed noise estimation
* Fixed entropy computation

siril 0.9.1
12/01/15

* added support for GSL 2
* fixed crash on startup without existing config file

siril 0.9.0
10/16/15

* new global star registration, taking into account field rotation
* new quality evaluation method for planetary images, used to sort the best
* images selected for stacking
* new parallelized stacking algorithm for all sequences, including all SER formats, allowing maximum used memory to be set
* threading of the most time consuming processings, to keep the GUI reactive, as well as many other speed improvements
* tested and improved on FreeBSD and MacOS X systems, and ARM architectures
* undo/redo on processing operations
* sequence cropping tool

siril 0.9.0rc1
12/29/14

* many fixes including background extraction, compositing alignment, rejection algorithm, wavelets
* threading of the heavy computations, to avoid graphical user interface freezing and provide a nice way of seeing what is happening in the console window
* image rotation with any angle (not in registration yet)
* new Canon banding removing tool
* GTK+ version is now 3.6 or above

siril 0.9.0b1
11/11/14

* new image formats supported for import and export (BMP, TIFF, JPEG, PNG, NetPBM, PIC (IRIS) RAW DSLR images)
* better image sequence handling with non-contiguous sequences, but still requiring file names to be postfixed by integers
* new graphical user interface based on GTK+ version 3.4 and above
* new display modes added to the standard linear scaling with lo/hi bounds
* manual translation as new registration method with two preview renderings of the current image with reference frame in transparency
* automatic translation as new registration method for deep-sky images, based on the PSF analysis of one star
* new commands available for the command line
* a star finding algorithm with PSF information
* new background extraction tool
* new processing functions
* new image compositing tool
* new stacking methods with rejection algorithms
* numerous bugs fixed and internal code refactoring

